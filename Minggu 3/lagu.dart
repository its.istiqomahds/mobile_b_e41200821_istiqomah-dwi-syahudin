void main(List<String>args) async {
  print(await mulai());
  print(await lirik1());
  print(await lirik2());
  print(await lirik3());
  print(await lirik4());
  print(await lirik5());
}
Future<String> mulai() async {
  String greeting = "mulai";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}
Future<String> lirik1() async {
  String greeting = "What would I do without your smart mouth";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}
Future<String> lirik2() async {
  String greeting = "Drawin' me in and you kicking me out";
  return await Future.delayed(Duration(seconds: 7), () => (greeting));
}
Future<String> lirik3() async {
  String greeting = "You've got my head spinning no kidding";
  return await Future.delayed(Duration(seconds: 6), () => (greeting));
}
Future<String> lirik4() async {
  String greeting = "What's going on in that beautiful mind";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}
Future<String> lirik5() async {
  String greeting = "I'm on your magical mystery ride";
  return await Future.delayed(Duration(seconds: 4), () => (greeting));
}